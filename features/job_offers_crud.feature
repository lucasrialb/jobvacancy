Feature: Job Offers CRUD
  In order to get employees
  As a job offerer
  I want to manage my offers

  Background:
  	Given I am logged in as job offerer

  Scenario: Create new offer
    When I create a new offer with "Programmer vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list

  Scenario: Create new offer with experience
    When I create a new offer with "Programmer vacancy" as the title and 3 years experience
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list 
    And I should see it has 3 years experience

  Scenario: Create new offer with no experience
    When I create a new offer with "Programmer vacancy" as the title and 0 years experience
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list
    And I should see it has not specified experience

  Scenario: Activate offer with experience
    Given I have "Programmer vacancy" offer with 3 years experience in my offers list 
    When I activate it
    Then I should see a offer activated confirmation message
    And I should see "Programmer vacancy" in latests offers list
    And I should see it has 3 years experience

  Scenario: Update offer
    Given I have "Programmer vacancy" offer in my offers list
    When I change the title to "Programmer vacancy!!!"
    Then I should see a offer updated confirmation message
    And I should see "Programmer vacancy!!!" in my offers list

  Scenario: Delete offer
    Given I have "Programmer vacancy" offer in my offers list
    When I delete it
    Then I should see a offer deleted confirmation message
# Wrong name, 'Programmer vacancy!!!' is never created
    And I should not see "Programmer vacancy!!!" in my offers list
