Sequel.migration do
  up do
    add_column :job_offers, :experience, String
  end

  down do
    drop_column :job_offers, :experience
  end
end
